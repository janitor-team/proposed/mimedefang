mimedefang (2.83-1) unstable; urgency=medium

    Terminology change:  Change "slave" to "worker" everywhere.

    *** NOTE INCOMPATIBILITY ***

    Check your init scripts and mimedefang.conf to make sure they use
    current names for shell	variables; a few "SLAVE" strings have been
    changed to "WORKER"

    mimedefang.pl: Add an extra level of subdirectories in the quarantine
    to avoid 32K subdirectory limit on ext3.

    *** NOTE INCOMPATIBILITY ***  Quarantine subdirectory naming changed.

 -- Christoph Martin <martin@uni-mainz.de>  Fri, 24 Nov 2017 16:15:39 +0100

mimedefang (2.73-2) unstable; urgency=low

    - /etc/default/mimedefang is no longer managed by dpkg. (See #688196)
      /usr/share/mimedefang/mimedefang.conf can be used as a reference.
    - The default of group writable mimedefang spool directory changed to
      not writable. This can be configure with the variable
      MD_ALLOW_GROUP_ACCESS in /etc/default/mimedefang. See
      /usr/share/mimedefang as a reference.

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Mon, 08 Oct 2012 12:38:02 +0200

mimedefang (2.56-1) unstable; urgency=low

    -  mimedefang.pl.in: The filter_begin function is now passed
    a single argument ($entity) representing the parsed message.

	    *** NOTE INCOMPATIBILITY ***

		filter_begin NOW TAKES ONE ARGUMENT,
		NOT ZERO.  IF YOUR FILTER HAS A
	        PROTOTYPE FOR filter_begin, YOU SHOULD
	        FIX OR REMOVE THE PROTOTYPE

    - mimedefang.c: Do NOT strip "bare CR" characters from e-mails by
    default.  The new "-c" command-line option enables the older behavior.

	    *** NOTE INCOMPATIBILITY ***

		WE NO LONGER STRIP BARE CR's FROM
	        MESSAGES BY DEFAULT.  TEST YOUR FILTERS
	        CAREFULLY TO MAKE SURE THEY CAN COPE
	        WITH THIS, OR USE THE -c FLAG.

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Mon, 13 Mar 2006 17:17:12 +0100

mimedefang (2.53-1) unstable; urgency=low

    -  mimedefang.pl: We don't detect and load Perl modules until
    the detect_and_load_perl_modules() function is called.

	    *** NOTE INCOMPATIBILITY ***

    You *MUST* call detect_and_load_perl_modules() inside your filter
    before you can rely on the %Features hash being set correctly,
    and before you can rely on SpamAssassin being loaded!!!

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Tue, 27 Sep 2005 17:26:15 +0200

mimedefang (2.48-1) unstable; urgency=low

    - Support for $Stupidity{"flatten"} removed.
	
	   *** NOTE INCOMPATIBILITY ***
	   
    - we try to preserve original multipart type of message.
      action_add_part now simply keeps a list
      of parts to be added.  At the end:

		a) If original message was multipart/mixed, we simply add
		the part.

		b) Otherwise, we make a new multipart/mixed container, put
		original message as the first part of this new container, and
		then add part to the multipart/mixed container.

	   *** NOTE INCOMPATIBILITY ***

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Fri, 12 Nov 2004 17:27:29 +0100

mimedefang (2.43-1) unstable; urgency=low

	   *** NOTE INCOMPATIBILITY ***

    Modified C and Perl code so that filter_relay is called
    when remote client connects rather than after MAIL FROM.
    This means the $helo argument is NOT available!

    filter_relay no longer has access to the HELO argument, nor
    does the MIMEDefang spool directory exist when filter_relay is called.

 -- Christoph Martin <christoph.martin@uni-mainz.de>  Mon,  5 Jul 2004 15:28:09 +0200

